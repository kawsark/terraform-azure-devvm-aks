output "devvm_public_ip" {
  value = azurerm_linux_virtual_machine.devvm.public_ip_address
}

output "devvm_private_key" {
  value       = tls_private_key.pem.private_key_pem
  description = "The private key for logging onto the server"
  sensitive   = true
}

output "devvm_public_key" {
  value       = tls_private_key.pem.public_key_openssh
  description = "The public key for the SSH key pair"
}
