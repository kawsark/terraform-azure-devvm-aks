resource "tls_private_key" "pem" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

resource "azurerm_ssh_public_key" "azure_private_key" {
  name                = var.ssh_key
  location            = azurerm_resource_group.aks_rg.location
  resource_group_name = azurerm_resource_group.aks_rg.name
  public_key          = tls_private_key.pem.public_key_openssh
}

