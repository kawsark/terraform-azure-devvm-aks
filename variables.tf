# TFC Organization
variable "tfc_org" {
  description = "This is the TFC Organization which hosts AKS with a kube_config output"
  default     = "GitlabCI-demos"
}

# TFC Workspace
variable "tfc_aks_workspace" {
  description = "This is the TFC Workspace which hosts AKS with a kube_config output"
  default     = "terraform-azure-aks-winnodepool"
}

# Azure Location
variable "location" {
  type        = string
  description = "Azure Region where all these resources will be provisioned"
  default     = "Central US"
}

# Azure Resource Group Name
variable "resource_group_name" {
  type        = string
  description = "This variable defines the Resource Group"
  default     = "terraform-aks"
}

# Azure AKS Environment Name
variable "environment" {
  type        = string
  description = "This variable defines the Environment"
  default     = "dev3"
}

# Add some tags
variable "tags" {
  type = map(string)
  default = {
    env   = "dev"
    TTL   = "48h"
    owner = "demouser"
  }
  description = "Tags that should be assigned to the resources in this example"
}

# SSH key variables
variable "ssh_key" {
  description = "The name of a SSH key pair to create in azure"
  default     = "demouser-dev-ssh_key"
}
