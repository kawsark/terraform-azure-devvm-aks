## terraform-azure-devvm-aks

The terraform configuration in this repo will create an Azure VM, install kubectl, and write out a Kubeconfig for cluster access. The objective is to provide a developer workstation to access a Kubernetes cluster. 

This repo also deploys a Windows and Linux application on the Kubernetes cluster via a startup script. For the interested reader, we have explained how the Kubeconfig configuration is read and passed into an Azure VM startup script in the Kubeconfig section below.

### How to use

- Provision an AKS cluster and export the `kube_config`: To provision the resources in this repo, you will need another TFC/TFE Workspace with the output `kube_config`. This is exported as an attribute from the [azurerm_kubernetes_cluster resource](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/kubernetes_cluster#kube_config). As an example, an AKS cluster can be provisioned using this repo: [terraform-azure-aks-winnodepoo](https://gitlab.com/kawsark/terraform-azure-aks-winnodepool). 

- Create a Workspace in TFC/TFE and associate it with your fork of this repo. Please see: [Connecting VCS Providers to Terraform Cloud](https://www.terraform.io/docs/cloud/vcs/index.html).

- Set Terraform Azure credentials as sensitive `ARM_*` environment variables. Please see: [Specify service principal credentials in environment variables](https://docs.microsoft.com/en-us/azure/developer/terraform/authenticate-to-azure?tabs=bash#specify-service-principal-credentials-in-environment-variables). Also: [Sensitive values](https://www.terraform.io/docs/cloud/workspaces/variables.html#sensitive-values)

- Create a Run in TFE/TFE as described here: [The UI- and VCS-driven Run Workflow](https://www.terraform.io/docs/cloud/run/ui.html).

- Once provisioning is completed, the `get_key.sh` script can be used for SSH access to the server. 
  - This script writes out the private key to access the server using the command: `terraform output -raw devvm_private_key`. 
  - Similarly, the IP address to access the server is also read using terraform: `terraform output -raw devvm_public_ip`
```bash
# Export the state file download URL, this can be accessed from TFC/TFE Workspace
export url=<state-file-download-url>

# Run the get_key.sh script to obtain the ssh command
./get_key.sh
SSH command is: ssh -i ./private_key.pem azureuser@$1.1.1.1
```

- Run `kubectl cluster-info` and `kubectl get pods` from the SSH session to interact with the cluster.

- To access the example applications, please run `kubectl get svc` and access the Load Balancer IP addresses via a web browser.

### Cleanup

- Queue a Destroy Run in your TFC/TFE Workspace and ensure that all resources are destroyed. 
- Delete the Workspace from TFC/TFE.

**Important:**
- Due to the `remote_state_datasource`, Terraform destroy must run for this Workspace **before** the Workspace with AKS cluster.
- Before Deleting the Workspace, please ensure there are zero resources reflected in the Workspace state file after the destroy Run. 

### Reading the kubeconfig

A Kubeconfig file is a YAML configuration that specifies how to access a Kubernetes cluster. This configuration will be obtained from another TFC/TFE Workspace using the `terraform_remote_state` Datasource (where the Kubernetes cluster is provisioned). 

Here is the snippet from [main.tf](main.tf) that shows the Datasource:
```bash
# Lookup kube_config
data "terraform_remote_state" "aks" {
  backend = "remote"

  config = {
    organization = var.tfc_org
    workspaces = {
      name = var.tfc_aks_workspace
    }
  }
}
```

Once the above Datasource is declared, the following code reads the `kube_config` output and passes it to a user data script called `custom_data` for `azurerm_linux_virtual_machine`.
```bash
# Lookup kube_config from AKS workspace
data "template_file" "startup_script" {
  template = file("${path.module}/azure-user-data.sh.tpl")
  vars = {
    kube_config_b64 = base64encode(data.terraform_remote_state.aks.outputs.kube_config)
  }
}
...

resource "azurerm_linux_virtual_machine" "devvm" {
  name                  = "devvm"
  ...
  custom_data = base64encode(data.template_file.startup_script.rendered)
...
```

